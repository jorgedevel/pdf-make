const express = require('express');
const app = express();
const cors = require('cors');
const PORT = 3030;
const path = require('path');

app.use(cors());
app.use(express.json())
app.use(express.static(path.join(__dirname, 'public')))
app.use('/report', require('./controller/report.controller'));

app.listen(PORT, () => {
    console.log('server run on port:', PORT);
})
